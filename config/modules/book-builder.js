module.exports = {
  instance: 'UCP',
  chapter: {
    dropdownValues: {
      back: ['Appendix A', 'Appendix B', 'Appendix C'],
      front: ['Table of Contents', 'Introduction', 'Preface'],
    },
  },
  stages: [
    {
      title: 'Upload',
      type: 'upload',
    },
    {
      title: 'File Prep',
      type: 'file_prep',
    },
    {
      title: 'Edit',
      type: 'edit',
    },
    {
      title: 'Review',
      type: 'review',
    },
    {
      title: 'Clean Up',
      type: 'clean_up',
    },
    {
      title: 'Page Check',
      type: 'page_check',
    },
    {
      title: 'Final',
      type: 'final',
    },
  ],
  divisions: [
    {
      name: 'Frontmatter',
      showNumberBeforeComponents: [],
      allowedComponentTypes: [
        { value: 'component', title: 'Component', predefined: true },
      ],
      defaultComponentType: 'component',
    },
    {
      name: 'Body',
      showNumberBeforeComponents: ['chapter'],
      allowedComponentTypes: [
        { value: 'chapter', title: 'Chapter', predefined: true },
        { value: 'part', title: 'Part', predefined: true },
        { value: 'unnumbered', title: 'Unnumbered', predefined: true },
      ],
      defaultComponentType: 'chapter',
    },
    {
      name: 'Backmatter',
      showNumberBeforeComponents: [],
      allowedComponentTypes: [
        { value: 'component', title: 'Component', predefined: true },
      ],
      defaultComponentType: 'component',
    },
  ],
  lockTrackChangesWhenReviewing: true,
}
