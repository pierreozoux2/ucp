#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const {
  model: ApplicationParameter,
} = require('editoria-data-model/src/applicationParameter')

const config = require('../config/modules/book-builder')

const seed = async () => {
  const areas = Object.keys(config)

  await Promise.all(
    areas.map(async area => {
      const parameters = await new ApplicationParameter({
        context: 'bookBuilder',
        area,
        config: JSON.stringify(config[area]),
      }).save()
      logger.info(
        `New Application Parameter created: ${JSON.stringify(config[area])}`,
      )
      return parameters
    }),
  )
}

seed()
